package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    Restaurant res = new Restaurant();

    @RequestMapping("/restaurant")
    public String getRestaurantList(@RequestParam(value="lat", defaultValue="") double lat, @RequestParam(value="log", defaultValue="") double log, @RequestParam(value="ray", defaultValue="") double ray) {
        return res.getRestaurantList(lat, log, ray, 10);
    }

    @RequestMapping("/restaurant/{{id}}")
    public String getRestaurantById(@PathVariable(value="{id}") String id) {
        return res.getRestaurantById(id);
    }
}
