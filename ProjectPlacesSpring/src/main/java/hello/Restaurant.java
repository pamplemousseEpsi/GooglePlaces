package hello;

import se.walkercrou.places.GooglePlaces;
import se.walkercrou.places.Place;
import se.walkercrou.places.exception.NoResultsFoundException;

import java.util.List;

/**
 * Created by Johan on 10/03/2017.
 */
public class Restaurant {

    String resultat = "";
    GooglePlaces client = new GooglePlaces("AIzaSyAm1FGLEKaVrmbR2LmZvLkRDJpSGvhte5Y");

    public String getRestaurantList(double lat, double log, double ray, int max){

        int nbRestaurant = 0;
        try{
            System.out.println("\nVeuillez patienter...\n");
            List<Place> places = client.getNearbyPlaces(lat, log, ray, max);

            for (Place p : places){
                for (String s : p.getTypes()){
                    if (s.equals(GooglePlaces.TYPE_RESTAURANT)){
                        //System.out.println(p.getName());
                        resultat += "Nom : " + p.getName() + "<br>Place Id : " + p.getPlaceId() + "<br><br>";
                        nbRestaurant++;

                        break;
                    }
                }
            }

            if(nbRestaurant == 0){
                System.out.println("Aucun restaurant trouvé");
                resultat = "Aucun restaurant trouvé";
            }
        }catch(NoResultsFoundException nrf){
            System.out.println("Aucun restaurant trouvé");
            resultat = "Aucun restaurant trouvé";
        }

        return resultat;
    }


    public String getRestaurantById(String id){

        try{
            System.out.println("\nVeuillez patienter...\n");
            Place place = client.getPlaceById(id);

            resultat += "Nom : " + place.getName() + "<br>Adresse : " + place.getAddress() + "<br><br>";

        }catch(NoResultsFoundException nrf){
            System.out.println("Aucun restaurant trouvé");
            resultat = "Aucun restaurant trouvé";
        }

        return resultat;
    }

}



