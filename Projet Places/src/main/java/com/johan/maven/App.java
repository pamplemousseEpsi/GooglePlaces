package com.johan.maven;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.sun.glass.ui.Application;
import com.sun.glass.ui.CommonDialogs;
import se.walkercrou.places.GooglePlaces;
import se.walkercrou.places.GooglePlacesInterface;
import se.walkercrou.places.Param;
import se.walkercrou.places.Place;
import se.walkercrou.places.exception.NoResultsFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        boolean isValide = true;
        boolean quitApp = false;
        int choix = 0;
        double latitude = 0.0;
        double longitude = 0.0;

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("\nRechercher par :\n1 : Adresse\n2 : Coordonnées\n\n3 : Quitter\n");

            do {
                isValide = true;
                choix = scanner.nextInt();
                switch (choix){
                    case 1 :
                        System.out.println("Adresse :");
                        String adresse = scanner.next();

                        try{
                            Geocoder geocoder = new Geocoder();
                            GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(adresse).getGeocoderRequest();
                            GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
                            List<GeocoderResult> results = geocoderResponse.getResults();
                            latitude = results.get(0).getGeometry().getLocation().getLat().doubleValue();
                            longitude = results.get(0).getGeometry().getLocation().getLng().doubleValue();
                        }catch (IndexOutOfBoundsException ioobe){
                            System.out.println("Adresse non valide");
                            System.out.println("\nRechercher par :\n1 : Adresse\n2 : Coordonnées\n\n3 : Quitter\n");
                            isValide = false;
                        }

                        break;
                    case 2 :
                        System.out.println("Latitude : ");
                        String temp = scanner.next();
                        if(temp.matches("\\d*\\.?,?\\d+")){
                            latitude = Double.parseDouble(temp.replace(",", ".")) ;
                        }else {
                            System.out.println("Format non valide !!");
                            System.out.println("\nRechercher par :\n1 : Adresse\n2 : Coordonnées\n\n3 : Quitter\n");
                            isValide = false;
                            break;
                        }

                        System.out.println("Longitude : ");
                        temp = scanner.next();
                        if(temp.matches("\\d*\\.?,?\\d+")) {
                            longitude = Double.parseDouble(temp.replace(",", "."));
                        }else {
                            System.out.println("Format non valide !!");
                            System.out.println("\nRechercher par :\n1 : Adresse\n2 : Coordonnées\n\n3 : Quitter\n");
                            isValide = false;
                            break;
                        }
                        break;
                    case 3 :
                        System.out.println("Fermeture de l'application");
                        System.exit(0);
                        break;
                    default:
                        System.out.print("Choix non valide\nChoix : ");
                        isValide = false;
                }
            }while (!isValide);

            System.out.println("Rayon de recherhce : ");
            Double ray = scanner.nextDouble();

            System.out.println("Nombre max de résultat : ");
            int maxResult = scanner.nextInt();

            GooglePlaces client = new GooglePlaces("AIzaSyAm1FGLEKaVrmbR2LmZvLkRDJpSGvhte5Y");

            int nbRestaurant = 0;
            try{
                System.out.println("\nVeuillez patienter...\n");
                List<Place> places = client.getNearbyPlaces(latitude, longitude, ray, maxResult);

                for (Place p : places){
                    for (String s : p.getTypes()){
                        if (s.equals(GooglePlaces.TYPE_RESTAURANT)){
                            System.out.println(p.getName());
                            nbRestaurant++;

                            break;
                        }
                    }
                }

                if(nbRestaurant == 0){
                    System.out.println("Aucun restaurant trouvé");
                }
            }catch(NoResultsFoundException nrf){
                System.out.println("Aucun restaurant trouvé");
            }

            System.out.println("\n1 : Nouvelle recherche\n\n2 : Quitter");

            do {
                choix = scanner.nextInt();
                isValide = true;

                switch (choix){
                    case 1 :
                        isValide = true;
                        break;
                    case 2 :
                        System.out.println("Fermeture de l'application");
                        System.exit(0);
                        break;
                    default:
                        System.out.print("Choix non valide\nChoix : ");
                        isValide = false;
                }
            }while (!isValide);

        }while (1 == 1);

    }
}
